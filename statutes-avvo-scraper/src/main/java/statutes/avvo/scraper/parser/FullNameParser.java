package statutes.avvo.scraper.parser;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FullNameParser {
    private static final String[] TRASH_SYMBOLS = {",", "^", "*"};
    private static final String[] TRASH_SYMBOLS_REPLACEMENTS = {"", "", ""};
    private static final String[] TRASH_PARTS = {"Ph.D.", "PhD", "Phd", "Mba", "Jr.", "Sr.", "Dr.", "Dr", "JD", "Jd"};

    private String firstName = "";
    private String middleName = "";
    private String lastName = "";

    public FullNameParser() {

    }

    public FullNameParser(String fullNameString) {
        parseFullNameString(fullNameString);
    }

    public void parseFullNameString(String fullNameString) {
        fullNameString = cleanUpName(fullNameString);

        final List<String> tokens = Arrays.stream(fullNameString.split("\\s+", 3))
                .map(String::trim)
                .collect(Collectors.toList());

        if (tokens.size() == 3) {
            this.firstName = tokens.get(0);
            this.middleName = tokens.get(1);
            this.lastName = tokens.get(2);
        }

        if (tokens.size() == 2) {
            this.firstName = tokens.get(0);
            this.lastName = tokens.get(1);
        }

        if (firstName.matches("[A-Z]\\.")) {
            final String buffer = middleName;
            middleName = firstName;
            firstName = buffer;
        }
    }

    /**
     * Returns original name part with every word capitalized.
     * Delimiters between name parts are suggested to be ' ', '-', '(' and ')'.
     */
    private String capitalizeNameParts(String originalNamePart) {

        return WordUtils.capitalizeFully(
                originalNamePart.toLowerCase(), ' ', '.', ',', '-', '(', ')', '\'', '’');
    }

    private String cleanUpName(String originalFullName) {

        originalFullName = StringUtils.replaceEach(originalFullName, TRASH_SYMBOLS, TRASH_SYMBOLS_REPLACEMENTS);

        for (String trash : TRASH_PARTS) {
            if (containsStandaloneWord(originalFullName, trash, true)){
                originalFullName = originalFullName.replace(trash, "");
            }
        }

        return originalFullName
                .replaceAll("\\(.*\\)", "")
                .replaceAll("\".*\"", "")
                .replaceAll("\\s+", " ")
                .replaceAll("“.*”", "")
                .replace(" .", "")
                .trim();
    }

    /**
     * Returns {@code true} if standalone occurrence of the word is found in an analysed
     * string. Occurrence must be at te very beginning or end of the string followed or
     * preceded accordingly by whitespace or to be inline substring surrounded by whitespaces.
     *
     * @param string - analysed string.
     * @param occurrence - word to find the standalone occurrence for.
     */
    public static boolean containsStandaloneWord(String string, String occurrence, boolean caseSensitive) {

        if (!caseSensitive) {
            string = string.toLowerCase();
            occurrence = occurrence.toLowerCase();
        }

        return string.matches(".*\\W" + occurrence + "\\W.*") ||
                string.matches(".*\\W" + occurrence + "$") ||
                string.matches("^" + occurrence + "\\W.*") ||
                string.matches("^" + occurrence + "$");
    }

    public void reset() {
        firstName = "";
        middleName = "";
        lastName = "";
    }

    public String getFirstName() {
        return capitalizeNameParts(firstName);
    }

    public String getMiddleName() {
        return capitalizeNameParts(middleName);
    }

    public String getLastName() {
        return capitalizeNameParts(lastName);
    }

}
