package statutes.avvo.scraper.parser;

import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;

import java.sql.Timestamp;
import java.util.*;

public class ParseHelper {

    public String getDate(String date, boolean before) {
        if (isYear(date)) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, Integer.parseInt(date));
            cal.set(Calendar.MONTH, before ? Calendar.JANUARY : Calendar.DECEMBER);
            cal.set(Calendar.DAY_OF_MONTH, before ? 1 : 31);
            return new Timestamp(cal.getTime().getTime()).toString();
        }
        final Parser parser = new Parser(TimeZone.getTimeZone("UTC"));
        final List<DateGroup> groups = parser.parse(date);
        Date date1 = groups
                .stream()
                .filter(Objects::nonNull)
                .map(item -> item.getDates()
                        .stream()
                        .filter(Objects::nonNull)
                        .findFirst().get()
                ).findFirst().get();
        return new Timestamp(date1.getTime()).toString();
    }

    public boolean isYear(String date) {
        return date.length() == 4;
    }
}
