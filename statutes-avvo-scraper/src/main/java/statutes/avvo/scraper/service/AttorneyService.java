package statutes.avvo.scraper.service;

import java.io.IOException;

/**
 * @author foxy
 * @since 24.07.18.
 */
public interface AttorneyService {
    void scrapeAttorney() throws IOException;
}
