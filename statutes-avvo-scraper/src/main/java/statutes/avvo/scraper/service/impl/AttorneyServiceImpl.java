package statutes.avvo.scraper.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import statutes.avvo.scraper.scraper.AttorneyScraper;
import statutes.avvo.scraper.service.AttorneyService;

import java.io.IOException;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Service
public class AttorneyServiceImpl implements AttorneyService {


    private final AttorneyScraper attorneyScraper;

    @Autowired
    public AttorneyServiceImpl(AttorneyScraper attorneyScraper) {
        this.attorneyScraper = attorneyScraper;
    }

    @Override
    public void scrapeAttorney() throws IOException {
//        attorneyScraper.prepareLinkForAttorneyScrape();
        attorneyScraper.scrapeAttorney();
    }
}
