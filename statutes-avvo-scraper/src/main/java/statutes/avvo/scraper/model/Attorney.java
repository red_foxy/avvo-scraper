package statutes.avvo.scraper.model;

import lombok.Data;
import lombok.ToString;
import statutes.avvo.scraper.model.url.URLContainer;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Entity
@Data
@ToString(exclude = {"licenseList", "practiceList", "awards","workExperienceList", "associationList", "educationList",
        "speakingEngagementsList", "profileLinkList"})
public class Attorney implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attorney_id")
    private Long id;

    @Column(name = "first_name")
    private String firstName = "";

    @Column(name = "last_name")
    private String lastName = "";

    @Column(name = "middle_name")
    private String middleName = "";

    @ElementCollection
    @CollectionTable(name = "phone_num")
    private List<String> phones = new ArrayList<>();

    private String website = "";

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToMany(mappedBy = "attorney", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<License> licenseList;

    @OneToMany(mappedBy = "attorney", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PracticeMakeup> practiceList;

    @ElementCollection
    @CollectionTable(name = "attorney_type")
    private List<String> attorneyTypes = new ArrayList<>();

    @Column(name = "miscoduct_found")
    private Boolean miscoductFound = false;

    @OneToMany(mappedBy = "attorney", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Award> awards;

    @OneToMany(mappedBy = "attorney", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<WorkExperience> workExperienceList;

    @OneToMany(mappedBy = "attorney", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Association> associationList;

    @OneToMany(mappedBy = "attorney", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Education> educationList;

    @OneToMany(mappedBy = "attorney", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SpeakingEngagements> speakingEngagementsList;

    @Lob
    private String bio = "";

    @Column(name = "paid_user")
    private Boolean paidUser = false;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "avvo_rating_id")
    private AvvoRating avvoRating;

    @OneToMany(mappedBy = "attorney", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProfileLink> profileLinkList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "url_container_id")
    private URLContainer urlContainer;

    @Column(name = "is_scraped")
    private Boolean isScraped = false;

}
