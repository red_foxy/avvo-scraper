package statutes.avvo.scraper.model.url;

import lombok.Data;
import lombok.ToString;
import statutes.avvo.scraper.model.Attorney;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Data
@Entity(name = "url_container")
@ToString(exclude = {"attorneyList"})
public class URLContainer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "state_abbreviation")
    private String stateAbbreviation;

    @Column(name = "attorney_type")
    private String attorneyType;

    private String url;

    @Column(name = "is_scraped")
    private Boolean isScraped = false;

    @OneToMany(mappedBy = "urlContainer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Attorney> attorneyList;
}
