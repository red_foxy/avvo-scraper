package statutes.avvo.scraper.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Entity(name = "work_experience")
@Data
@ToString(exclude = {"attorney"})
public class WorkExperience implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title = "";

    @Column(name = "company_name")
    private String companyName = "";

    @Column(name = "year_start")
    private Integer yearStart;

    @Column(name = "year_end")
    private Integer yearEnd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attorney_id")
    private Attorney attorney;
}
