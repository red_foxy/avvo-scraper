package statutes.avvo.scraper.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Entity
@Data
@ToString(exclude = {"attorney"})
public class License implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "license_state")
    private String licenseState = "";

    @Column(name = "license_year_acquired")
    private Integer licenseYearAcquired;

    @Column(name = "license_active")
    private String licenseActive = "";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attorney_id")
    private Attorney attorney;
}
