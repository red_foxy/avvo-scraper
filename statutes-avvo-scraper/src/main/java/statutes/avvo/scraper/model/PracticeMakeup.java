package statutes.avvo.scraper.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Entity(name = "practice_makeup")
@Data
public class PracticeMakeup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "practice_type")
    private String practiceType = "";

    private String years = "";

    private String percentage = "";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attorney_id")
    private Attorney attorney;

}
