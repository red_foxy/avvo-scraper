package statutes.avvo.scraper.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Entity
@Data
public class Education implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "school_name")
    private String schoolName = "";

    private String degree = "";

    @Column(name = "year_graduated")
    private Integer yearGraduated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attorney_id")
    private Attorney attorney;
}


