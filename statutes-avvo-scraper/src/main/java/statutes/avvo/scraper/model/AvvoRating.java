package statutes.avvo.scraper.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Entity(name = "avvo_rating")
@Data
@ToString(exclude = {"attorney"})
public class AvvoRating implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "avvo_rating_id")
    private Long id;

    @Column(name = "num_5_stars")
    private Integer num5Stars;

    @Column(name = "num_4_stars")
    private Integer num4Stars;

    @Column(name = "num_3_stars")
    private Integer num3Stars;

    @Column(name = "num_2_stars")
    private Integer num2Stars;

    @Column(name = "num_1_stars")
    private Integer num1Stars;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "avvoRating")
    private Attorney attorney;
}
