package statutes.avvo.scraper.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Entity
@Data
@ToString(exclude = {"attorney"})
public class Address implements Serializable {

    private static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private Long id;

    @Column(name = "street_address_num")
    private String streetAddressNum = "";

    @Column(name = "street_name")
    private String streetName = "";

    @Column(name = "apt_suite_num")
    private String aptSuiteNum = "";

    private String city = "";

    private String state = "";

    private String zipcode = "";

    private String latitude = "";

    private String longitude = "";

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "address")
    private Attorney attorney;
}
