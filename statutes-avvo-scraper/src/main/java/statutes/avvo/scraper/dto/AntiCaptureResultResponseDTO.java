package statutes.avvo.scraper.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author foxy
 * @since 31.07.18.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AntiCaptureResultResponseDTO implements Serializable {

    @JsonProperty("errorId")
    private Integer errorId;

    @JsonProperty("status")
    private String status;

    @JsonProperty("solution")
    private GRecaptchaResponseDTO solution;

    @JsonProperty("cost")
    private String cost;

    @JsonProperty("ip")
    private String ip;

    @JsonProperty("createTime")
    private Integer createTime;

    @JsonProperty("endTime")
    private Integer endTime;

    @JsonProperty("solveCount")
    private Integer solveCount;
}
