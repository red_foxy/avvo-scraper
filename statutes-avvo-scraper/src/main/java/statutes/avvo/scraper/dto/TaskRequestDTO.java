package statutes.avvo.scraper.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author foxy
 * @since 31.07.18.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskRequestDTO {

    @JsonProperty("type")
    private String type;

    @JsonProperty("websiteURL")
    private String websiteURL;

    @JsonProperty("websiteKey")
    private String websiteKey;

}
