package statutes.avvo.scraper.scraper;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import statutes.avvo.scraper.constant.ConfigProperties;
import statutes.avvo.scraper.driver.WebDriverWrapper;
import statutes.avvo.scraper.dto.*;
import statutes.avvo.scraper.model.*;
import statutes.avvo.scraper.model.url.URLContainer;
import statutes.avvo.scraper.parser.FullNameParser;
import statutes.avvo.scraper.repository.AttorneyRepository;
import statutes.avvo.scraper.repository.URLContainerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.slf4j.LoggerFactory.getLogger;
import static statutes.avvo.scraper.constant.Constants.AVVO_START_PAGE;

/**
 * @author foxy
 * @since 24.07.18.
 */
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class AttorneyScraper {

    private final Logger logger = getLogger(AttorneyScraper.class);
    private final WebDriverWrapper driver;
    private final ConfigProperties configProperties;
    private final URLContainerRepository urlContainerRepository;
    private final AttorneyRepository attorneyRepository;
    private final WebDriverWait wait;

    private static final String STATE_SELECTOR = "#js-top-state-link-farm a";
    private static final String PRACTICE_AREAS_SELECTOR = "#areas-of-law a";
    private static final String DOCUMENT_URL_PART = "https://www.avvo.com/find-a-lawyer/all-practice-areas/al";
    private static final String MAIN_URL_PART = "https://www.avvo.com";
    private static final String AVVO_LOGIN_PAGE = "https://www.avvo.com/account/login";

    private static final String ANTI_CAPTURE_CREATE_TASK_URL = "http://api.anti-captcha.com/createTask";
    private static final String ANTI_CAPTURE_RESPONSE_URL = "https://api.anti-captcha.com/getTaskResult";
    private static final String CLIENT_KEY = "551613801b17c7aebdad01056757216b";
    private static final String TYPE = "NoCaptchaTaskProxyless";

    @Autowired
    public AttorneyScraper(WebDriverWrapper driver,
                           ConfigProperties configProperties,
                           URLContainerRepository urlContainerRepository,
                           AttorneyRepository attorneyRepository) {
        this.driver = driver;
        this.configProperties = configProperties;
        this.urlContainerRepository = urlContainerRepository;
        this.attorneyRepository = attorneyRepository;
        this.wait = new WebDriverWait(driver.getWebDriver(), 60);
    }

    public void scrapeAttorney() {
        boolean scrapingDone = false;
//        do {
//            try {
        scrapingDone = startScrapeAttorney();
//            } catch (Exception e) {
//
//                logger.error("SCRAPING FAILED, TRYING TO RESTART...\n");
//            }
//        } while (!scrapingDone);
//
//        logger.info("# Scraping process finished");
    }

    private boolean startScrapeAttorney() {

        boolean scrapingDone = false;
        List<URLContainer> urlContainerList = urlContainerRepository.findAllByIsScrapedFalse();

        for (URLContainer container : urlContainerList) {
            scrapingDone = populateMatchingAttorneyByLink(container);
        }
        return scrapingDone;
    }

    public void prepareLinkForAttorneyScrape() {
        logger.info("# Link scraping process initialized");

        driver.getWebDriver().get(AVVO_LOGIN_PAGE);
        if (driver.exists(By.xpath("//*[@id=\"new_sign_in\"]/div[4]/div[1]/button"))) {
            login();
        }

        List<URLContainer> urlContainerList = new ArrayList<>();
        for (String stateAbbreviation : getAbbreviationsForLinkGeneration()) {
            for (String attorneyType : getAttorneyTypeForLinkGeneration()) {
                String link = getLink(attorneyType, stateAbbreviation);
                final URLContainer container = new URLContainer();
                container.setStateAbbreviation(stateAbbreviation);
                container.setAttorneyType(attorneyType);
                container.setUrl(link);
                urlContainerList.add(container);
                break;
            }
        }
        urlContainerRepository.saveAll(urlContainerList);
        logger.info("# Link scraping process finished");
    }

    private List<String> getAbbreviationsForLinkGeneration() {
        driver.getWebDriver().get(AVVO_START_PAGE);
        final List<WebElement> stateLinks = driver.getWebDriver().findElements(By.cssSelector(STATE_SELECTOR));
        List<String> stateAbbreviations = new ArrayList<>();
        for (WebElement stateLink : stateLinks) {
            String rawStateLink = stateLink.getAttribute("href");
            String stateAbbreviation = StringUtils.substringBetween(rawStateLink, "/all-lawyers/", ".html");
            stateAbbreviations.add(stateAbbreviation);
        }

        return stateAbbreviations;
    }

    private List<String> getAttorneyTypeForLinkGeneration() {
        driver.getWebDriver().get(DOCUMENT_URL_PART);
        final List<WebElement> practiceAreasLinks = driver.getWebDriver().findElements(By.cssSelector(PRACTICE_AREAS_SELECTOR));
        List<String> attorneyTypes = new ArrayList<>();
        for (WebElement typeLink : practiceAreasLinks) {
            String rawPracticeAreaLink = typeLink.getAttribute("href");
            String attorneyType = StringUtils.substringBetween(rawPracticeAreaLink, "https://www.avvo.com/", "/al.html");
            attorneyTypes.add(attorneyType);
        }
        return attorneyTypes;
    }

    private String getLink(String type, String stateAbbreviation) {
        return MAIN_URL_PART + "/" + type + "/" + stateAbbreviation + ".html";
    }

    private void login() {
        logger.info("# Logging in");
        driver.getWebDriver().manage().window().maximize();
        driver.sendKeys(By.id("sign_in_email"), configProperties.getAvvoLogin());
        driver.sendKeys(By.id("sign_in_password"), configProperties.getAvvoPassword());
        driver.getWebDriver().findElement(By.xpath("//*[@id=\"new_sign_in\"]/div[4]/div[1]/button")).click();
    }

    private boolean populateMatchingAttorneyByLink(URLContainer container) {
        if (driver.exists(By.cssSelector(".g-recaptcha"))) {
            String dataSitekey = driver.getWebDriver().findElement(By.cssSelector("body > section > div.content-wrapper > div > div.g-recaptcha")).getAttribute("data-sitekey");
            System.err.println("dataSitekey" + dataSitekey);
            AntiCaptureCreateTaskResponseDTO taskResponseDTO = createAntiCaptureTask(driver.getWebDriver().getCurrentUrl(), dataSitekey);
            AntiCaptureResultResponseDTO captureResultResponseDTO = getAntiCaptureResponse(taskResponseDTO);
            makeSomeMagic(captureResultResponseDTO.getSolution().getGRecaptchaResponse());
        }
        driver.delay(2000, 2500);
        driver.getWebDriver().get(container.getUrl());

        By policySelector = By.cssSelector("div.gdpr-banner.avvo-banner-overrides.ibeugdpr--center > p:nth-child(3) > a");
        if (driver.exists(policySelector)) {
            driver.delay(4000, 6000);
            wait.until(ExpectedConditions.visibilityOfElementLocated(policySelector)).click();
            driver.delay(4000, 6000);
        }

        List<Attorney> attorneyList = new ArrayList<>();
        while (true) {
            if (driver.exists(By.cssSelector("div[data-gtm-context=\"organic results\"]"))) {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[data-gtm-context=\"organic results\"]")));
                driver.scrollToElement(By.cssSelector(".footer-nav"));
                driver.delay(4000, 6000);

                List<WebElement> attorneys = new ArrayList<>();
                attorneys.addAll(driver.getWebDriver().findElements(By.cssSelector("li.fsj-optimizely-original-new-layout")));

                for (WebElement attorney : attorneys) {
                    Attorney bio = new Attorney();
                    WebElement userLinkElement = attorney.findElement(By.cssSelector(".v-serp-block-link"));
                    String userLink = userLinkElement.getAttribute("href");
                    ProfileLink profileLink = new ProfileLink();
                    profileLink.setAvvoLink(userLink);
                    profileLink.setAttorney(bio);
                    List<ProfileLink> attorneyProfileLinkList = new ArrayList<>();
                    attorneyProfileLinkList.add(profileLink);
                    bio.setProfileLinkList(attorneyProfileLinkList);
                    List<String> attorneyTypesList = new ArrayList<>();
                    attorneyTypesList.add(container.getAttorneyType());
                    bio.setAttorneyTypes(attorneyTypesList);
                    attorneyList.add(bio);
                }

                By nextPageCssSelector = By.cssSelector(".pagination-next a");
                if (driver.exists(nextPageCssSelector)) {
                    WebElement nextPageButton = driver.getWebDriver().findElement(nextPageCssSelector);
                    driver.delay(4000, 5000);
                    try {
                        nextPageButton.click();
                    } catch (Exception e) {
                        driver.getWebDriver().get(container.getUrl());
                    }
                    driver.delay(4000, 5000);
                } else {
                    break;
                }
            } else {
                container.setIsScraped(Boolean.TRUE);
                urlContainerRepository.save(container);
                return true;
            }
        }

        container.setAttorneyList(attorneyList);
        attorneyList.forEach(item -> item.setUrlContainer(container));
        container.setIsScraped(Boolean.TRUE);
        urlContainerRepository.save(container);
        return true;
    }

    @Transactional
    public void scrapeProfile(URLContainer container) {
        driver.getWebDriver().manage().window().maximize();

        for (Attorney attorney : container.getAttorneyList()) {
            if (attorney.getIsScraped() != Boolean.TRUE) {
                driver.getWebDriver().get(attorney.getProfileLinkList().get(0).getAvvoLink());
                driver.delay(4000, 6000);

                acceptPrivacyPolicy();

                driver.delay(4000, 6000);

                driver.scrollToElement(By.cssSelector(".footer-nav"));

                By lawyerFullContainerTypeSelector = By.cssSelector(".v-js-lawyer-container");
                By lawyerDowngradedContainerTypeSelector = By.cssSelector(".downgraded-card-body");

                By standartFullNameSelector;
                if (driver.exists(lawyerFullContainerTypeSelector)) {
                    // scrape full profile
                    scrapeFullTypeAttorney(attorney);
                } else if (driver.exists(lawyerDowngradedContainerTypeSelector)) {
                    scrapeDowngradedTypeAttorney(attorney);
                } else {
                    System.out.println("Something else...");
                }

                attorney.setIsScraped(Boolean.TRUE);
                attorneyRepository.save(attorney);
                attorneyRepository.findAll();
            }
        }

    }

    private List<String> generateReviewsLink(String avvoLink) {
        List<String> reviewLinkList = new ArrayList<>();
        for (int i = 1; i < 6; i++) {
            String linkMainPart = StringUtils.substringBefore(avvoLink, ".html");
            String reviewLink = linkMainPart + "/reviews.html?rating=" + i;
            reviewLinkList.add(reviewLink);
        }
        return reviewLinkList;
    }

    private void scrapeRating(Attorney attorney) {
        AvvoRating avvoRating = new AvvoRating();
        List<String> reviewLinkList = generateReviewsLink(attorney.getProfileLinkList().get(0).getAvvoLink());
        List<Integer> reviewValueList = new ArrayList<>();
        for (int i = 0; i < reviewLinkList.size(); i++) {
            driver.getWebDriver().get(reviewLinkList.get(i));
            driver.delay(4000, 6000);

            String fullReviewInfo = driver.getWebDriver()
                    .findElement(By.cssSelector("div.off-canvas-container > div > div > div > div.container.u-vertical-padding-2 > div > div > div:nth-child(2) > div"))
                    .getText();

            String reviewValueString = StringUtils.substringBetween(fullReviewInfo, "of ", " review");
            reviewValueList.add(Integer.parseInt(reviewValueString));
        }
        avvoRating.setAttorney(attorney);
        avvoRating.setNum1Stars(reviewValueList.get(0));
        avvoRating.setNum2Stars(reviewValueList.get(1));
        avvoRating.setNum3Stars(reviewValueList.get(2));
        avvoRating.setNum4Stars(reviewValueList.get(3));
        avvoRating.setNum5Stars(reviewValueList.get(4));
        attorney.setAvvoRating(avvoRating);
    }

    private void acceptPrivacyPolicy() {
        By policySelector = By.cssSelector("div.gdpr-banner.avvo-banner-overrides.ibeugdpr--center > p:nth-child(3) > a");
        if (driver.exists(policySelector)) {
            driver.delay(4000, 6000);
            wait.until(ExpectedConditions.visibilityOfElementLocated(policySelector)).click();
            driver.delay(4000, 6000);
        }
    }

    private void scrapeFullTypeAttorney(Attorney attorney) {
        By fullNameSelector = By.cssSelector(".v-js-lawyer-container .overridable-lawyer-phone h1 > span[itemprop = name]");
        parseFullName(attorney, fullNameSelector);

        By phoneNumberSelector = By.xpath("/html/body/div[1]/div/div/div[4]/div[2]/div/div[2]/div/div[1]/div/div/div/div/div/div[3]/span[1]/div/div[2]/span/a/span[2]");
        parsePhoneNumber(attorney, phoneNumberSelector);

        By websiteSelector = By.cssSelector(".v-lawyer-card-wrapper span.hidden-xs.visible-sm.visible-md.visible-lg div:nth-child(3) > a");
        scrapeWebsite(attorney, websiteSelector);

        scrapeAddress(attorney, true);
        scrapeLicenses(attorney);

        By practiceAreasDonutSelector = By.cssSelector("#practice_areas  ol > li");
        scrapePracticeAreas(attorney, practiceAreasDonutSelector);

        By miscoductSelector = By.cssSelector("#sanctions > div > p > svg[data-icon=exclamation-triangle]");
        scrapeMiscoduct(attorney, miscoductSelector);

        By showFullResumeSelector = By.cssSelector("#resume > div > div > div > div > div:nth-child(2) > p > a");
        if (showFullResume(showFullResumeSelector)) {
            By awardSelector = By.cssSelector("#js-v-full-resume > div:nth-child(1) > table > tbody");
            scrapeAwards(attorney, awardSelector);

            By workExperienceSelector = By.cssSelector("#js-v-full-resume > div:nth-child(2) > table > tbody");
            scrapeWorkExperience(attorney, workExperienceSelector);

            By associationSelector = By.cssSelector("#js-v-full-resume > div:nth-child(3) > table > tbody");
            scrapeAssociations(attorney, associationSelector);

            // fix with num ATTENTION!
            int sectionNumber = 4;
            sectionNumber = scrapeEducationList(attorney, sectionNumber);

            By speakingEngagementSelector = By.cssSelector("#js-v-full-resume > div:nth-child(" + sectionNumber + ") > table > tbody");
            scrapeSpeakingEngagement(attorney, speakingEngagementSelector, sectionNumber);

            By aboutAttorneySelector = By.cssSelector("#about");
            scrapeBio(attorney, aboutAttorneySelector);

            By proStatusSelector = By.cssSelector("div.v-js-lawyer-container div.v-lawyer-card-wrapper div.remove-right-gutter > div > div");
            scrapePaidAccount(attorney, proStatusSelector);

            By reviewsSelector = By.cssSelector("section#client_reviews");
            scrapeReviewsBySelector(attorney, reviewsSelector);
        }
    }

    private void scrapeDowngradedTypeAttorney(Attorney attorney) {
        By fullNameSelector = By.cssSelector("div.downgraded-card-body div > h2");
        parseFullName(attorney, fullNameSelector);

        By phoneNumberSelector = By.xpath("/html/body/div[1]/div/div/div[4]/div[2]/div/div[2]/div/div[1]/div/div/div/div/div/div[3]/span[1]/div/div[2]/span/a/span[2]");
        parsePhoneNumber(attorney, phoneNumberSelector);

        scrapeAddress(attorney, false);
        scrapeLicenses(attorney);

        scrapePracticeAreasDowngrade(attorney);

        By miscoductSelector = By.cssSelector("#sanctions > div > p > svg[data-icon=exclamation-triangle]");
        scrapeMiscoduct(attorney, miscoductSelector);

        By showFullResumeSelector = By.cssSelector("div.off-canvas-container > div > div > div:nth-child(7) > div:nth-child(2) > div.col-xs-12.col-md-8 > div:nth-child(4) > div > div:nth-child(9) > div > div > div:nth-child(2) > p > a");
        if (showFullResume(showFullResumeSelector)) {
            By awardSelector = By.cssSelector("#js-v-full-resume > div:nth-child(1) > table > tbody");
            scrapeAwards(attorney, awardSelector);

            By workExperienceSelector = By.cssSelector("#js-v-full-resume > div:nth-child(2) > table > tbody");
            scrapeWorkExperience(attorney, workExperienceSelector);

            By associationSelector = By.cssSelector("#js-v-full-resume > div:nth-child(3) > table > tbody");
            scrapeAssociations(attorney, associationSelector);

            // fix with num ATTENTION!
            scrapeEducationList(attorney, 4);
            By speakingEngagementSelector = By.cssSelector("#js-v-full-resume > div:nth-child(5) > table > tbody");
            scrapeSpeakingEngagement(attorney, speakingEngagementSelector, 5);

            By aboutAttorneySelector = By.cssSelector("#about");
            scrapeBio(attorney, aboutAttorneySelector);

            By proStatusSelector = By.cssSelector("div.v-js-lawyer-container div.v-lawyer-card-wrapper div.remove-right-gutter > div > div");
            scrapePaidAccount(attorney, proStatusSelector);

            By reviewsSelector = By.cssSelector("section#client_reviews");
            scrapeReviewsBySelector(attorney, reviewsSelector);
        }
    }

    private void parseFullName(Attorney attorney, By fullNameSelector) {
        FullNameParser fullNameParser = new FullNameParser("");
        String fullName = driver.getWebDriver().findElement(fullNameSelector).getText();
        driver.delay(2000, 2500);
        fullNameParser.parseFullNameString(fullName);
        attorney.setFirstName(fullNameParser.getFirstName());
        attorney.setLastName(fullNameParser.getLastName());
        attorney.setMiddleName(fullNameParser.getMiddleName());
        fullNameParser.reset();
        driver.delay(4000, 6000);
    }

    private void parsePhoneNumber(Attorney attorney, By phoneNumberSelector) {
        if (driver.exists(phoneNumberSelector)) {
            driver.scrollToElement(phoneNumberSelector);
            String phoneNumber = driver.getWebDriver().findElement(phoneNumberSelector).getText();
            System.err.println("phoneNumber " + phoneNumber);
            List<String> phoneNumberList = new ArrayList<>();
            phoneNumberList.add(phoneNumber);
            attorney.setPhones(phoneNumberList);
        }
    }

    private void scrapeWebsite(Attorney attorney, By websiteSelector) {
        if (driver.exists(websiteSelector)) {
            driver.scrollToElement(websiteSelector);
            WebElement websiteElement = driver.getWebDriver().findElement(websiteSelector);
            String websiteLink = websiteElement.getAttribute("href");
            System.err.println("websiteLink " + websiteLink);
            attorney.setWebsite(websiteLink);
        }
    }

    private void scrapeAddress(Attorney attorney, boolean isFullType) {
        Address address = new Address();

        By addressSelector = By.cssSelector("#contact div > span > p > span:nth-child(1)");
        String addressPart = "";
        String streetAddressNum = "";
        if (driver.exists(addressSelector)) {
            driver.scrollToElement(addressSelector);
            addressPart = driver.getWebDriver().findElement(addressSelector).getText();
        } else {
            addressSelector = By.cssSelector(".js-address-container address > span > p:nth-child(1) > span:nth-child(1)");
            driver.scrollToElement(addressSelector);
            addressPart = driver.getWebDriver().findElement(addressSelector).getText();
            if (addressPart.contains("\n")) {
                addressPart = StringUtils.substringBefore(addressPart, "\n");
            }
        }

        System.err.println("addressPart" + addressPart);
        if (!addressPart.equals("")) {
            Pattern p = Pattern.compile("\\d+\\s+");
            Matcher m = p.matcher(addressPart);
            m.find();
            streetAddressNum = m.group();
        }

        String streetName = addressPart.replaceAll("\\d+\\s+", "");
        address.setAttorney(attorney);
        address.setStreetAddressNum(streetAddressNum);
        address.setStreetName(streetName);
//        div.u-vertical-padding-half > div > address > span > p:nth-child(1) > span:nth-child(1)
        if (driver.exists(By.cssSelector("div.u-vertical-padding-half > div > address > span > p:nth-child(1) > span:nth-child(1) > span"))) {
            String aptSuiteNum = driver.getWebDriver()
                    .findElement(By.cssSelector("div.u-vertical-padding-half > div > address > span > p:nth-child(1) > span:nth-child(1) > span"))
                    .getText();
            address.setAptSuiteNum(aptSuiteNum);
        }
        String addressPartTag = "";
        if (!isFullType) {
            addressPartTag = "address >";
        }
        String city = driver.getWebDriver()
                .findElement(By.cssSelector("div.u-vertical-padding-half > div > " + addressPartTag + " span > p:nth-child(1) > span:nth-child(2)")).getText();
        address.setCity(city);
        String state = driver.getWebDriver()
                .findElement(By.cssSelector("div.u-vertical-padding-half > div > " + addressPartTag + " span > p:nth-child(1) > span:nth-child(4)")).getText();
        address.setState(state);
        String zipcode = driver.getWebDriver()
                .findElement(By.cssSelector("div.u-vertical-padding-half > div > " + addressPartTag + " span > p:nth-child(1) > span:nth-child(6)")).getText();
        address.setZipcode(zipcode);
        String latitude = driver.getWebDriver().findElement(By.cssSelector("address")).getAttribute("data-latitude");
        address.setLatitude(latitude);
        String longitude = driver.getWebDriver().findElement(By.cssSelector("address")).getAttribute("data-longitude");
        address.setLongitude(longitude);

        attorney.setAddress(address);
    }

    private void scrapeLicenses(Attorney attorney) {
        By licenseSelector = By.cssSelector("div > div > div > div > div:nth-child(1) > div.col-xs-12.col-sm-7 > table > tbody");
        if (driver.exists(licenseSelector)) {
            driver.scrollToElement(licenseSelector);
            List<License> licenseList = new ArrayList<>();
            List<WebElement> licenseElementList = driver.getWebDriver()
                    .findElements(By.cssSelector("div > div > div > div > div:nth-child(1) > div.col-xs-12.col-sm-7 > table > tbody > tr"));
            for (WebElement licenseElement : licenseElementList) {
                License license = new License();
                license.setAttorney(attorney);
                String licenseState = licenseElement.findElement(By.cssSelector("td[data-title=\"State\"]")).getText();
                license.setLicenseState(licenseState);
                String licenseYearAcquired = licenseElement.findElement(By.cssSelector("td[data-title=\"Origin\"]")).getText();
                if (licenseYearAcquired.equals("")) {
                    license.setLicenseYearAcquired(-1);
                } else {
                    license.setLicenseYearAcquired(Integer.parseInt(licenseYearAcquired));
                }
                String licenseActive = licenseElement.findElement(By.cssSelector("td[data-title=\"Status\"]")).getText();
                license.setLicenseActive(licenseActive);
                licenseList.add(license);
            }
            attorney.getLicenseList().clear();
            attorney.getLicenseList().addAll(licenseList);
        }
    }

    private void scrapePracticeAreas(Attorney attorney, By practiceAreasDonutSelector) {
        List<PracticeMakeup> practiceMakeupList = new ArrayList<>();
        if (driver.exists(practiceAreasDonutSelector)) {
            driver.scrollToElement(By.cssSelector("#practice_areas  ol > li"));
            List<WebElement> practiceElementList = driver.getWebDriver().findElements(By.cssSelector("#practice_areas  ol > li"));
            for (WebElement practiceElement :
                    practiceElementList) {
                PracticeMakeup practiceMakeup = new PracticeMakeup();
                practiceMakeup.setAttorney(attorney);
                String practiceWithPercent = practiceElement.findElement(By.cssSelector("a")).getText();
                if (!practiceWithPercent.equals("")) {
                    String practiceType = StringUtils.substringBefore(practiceWithPercent, ":");
                    String percentageString = StringUtils.substringBetween(practiceWithPercent, ": ", "%");
                    if (!percentageString.equals("")) {
                        float percentage = (Float.parseFloat(percentageString) / 100);
                        String percentageRaw = String.valueOf(percentage);
                        String formattedPercentage = StringUtils.substringAfter(percentageRaw, "0");
                        practiceMakeup.setPercentage(formattedPercentage);
                    }
                    practiceMakeup.setPracticeType(practiceType);
                }
                By practiceYearSelector = By.cssSelector("#practice_areas p.small.text-muted");
                if (driver.exists(practiceYearSelector)) {
                    String yearRaw = driver.getWebDriver().findElement(practiceYearSelector).getText();
                    String years = StringUtils.substringBefore(yearRaw, " years");
                    practiceMakeup.setYears(years);
                }
                practiceMakeupList.add(practiceMakeup);
            }
            attorney.getPracticeList().clear();
            attorney.getPracticeList().addAll(practiceMakeupList);
        }
    }

    private void scrapePracticeAreasDowngrade(Attorney attorney) {
        List<PracticeMakeup> practiceMakeupList = new ArrayList<>();
        driver.scrollToElement(By.cssSelector("#practice-areas > p"));
        String practiceAreasRaw = driver.getWebDriver().findElement(By.cssSelector("#practice-areas > p")).getText();
        String[] practiceAreasByComma = practiceAreasRaw.split(",");
        for (String rawPracticeArea : practiceAreasByComma) {
            PracticeMakeup practiceMakeup = new PracticeMakeup();
            practiceMakeup.setAttorney(attorney);
            practiceMakeup.setPracticeType(rawPracticeArea);
            practiceMakeupList.add(practiceMakeup);
        }
        attorney.getPracticeList().clear();
        attorney.getPracticeList().addAll(practiceMakeupList);
    }

    private void scrapeMiscoduct(Attorney attorney, By miscoductSelector) {

        if (driver.exists(miscoductSelector)) {
            attorney.setMiscoductFound(Boolean.TRUE);
        }
    }

    private boolean showFullResume(By showFullResumeSelector) {
        if (driver.exists(showFullResumeSelector)) {
            driver.delay(2000, 2500);
            wait.until(ExpectedConditions.presenceOfElementLocated(showFullResumeSelector));
            driver.delay(2000, 2500);
            wait.until(ExpectedConditions.visibilityOfElementLocated(showFullResumeSelector)).click();
            driver.delay(5000, 7000);
            return true;
        }
        return false;
    }

    private void scrapeAwards(Attorney attorney, By awardSelector) {
        if (driver.exists(awardSelector)) {

            By moreSelector = By.cssSelector("#js-v-full-resume > div:nth-child(1) > table > tfoot > tr > td > a");
            clickMore(moreSelector);

            List<Award> awardList = new ArrayList<>();
            List<WebElement> awardElementList = driver.getWebDriver().findElements(By.cssSelector("#js-v-full-resume > div:nth-child(1) > table > tbody > tr"));
            for (WebElement awardElement : awardElementList) {
                Award award = new Award();
                award.setAttorney(attorney);
                String awardName = awardElement.findElement(By.cssSelector("th[scope=\"row\"]")).getText();
                award.setAwardName(awardName);
                String grantor = awardElement.findElement(By.cssSelector("td[data-title=\"Grantor\"]")).getText();
                award.setGrantor(grantor);
                String yearGranted = awardElement.findElement(By.cssSelector("td[data-title=\"Date granted\"]")).getText();
                award.setYearGranted(Integer.parseInt(yearGranted));
                awardList.add(award);
            }
            attorney.getAwards().clear();
            attorney.getAwards().addAll(awardList);
        }
    }

    private void scrapeWorkExperience(Attorney attorney, By workExperienceSelector) {

        if (driver.exists(workExperienceSelector)) {

            By moreSelector = By.cssSelector("#js-v-full-resume > div:nth-child(2) > table > tfoot > tr > td > a");
            clickMore(moreSelector);

            List<WorkExperience> workExperienceList = new ArrayList<>();
            List<WebElement> workElementList = driver.getWebDriver().findElements(By.cssSelector("#js-v-full-resume > div:nth-child(2) > table > tbody > tr"));
            for (WebElement workElement : workElementList) {
                WorkExperience workExperience = new WorkExperience();
                workExperience.setAttorney(attorney);
                String title = workElement.findElement(By.cssSelector("th[scope=\"row\"]")).getText();
                workExperience.setTitle(title);
                String companyName = workElement.findElement(By.cssSelector("td[data-title=\"Company name\"]")).getText();
                workExperience.setCompanyName(companyName);
                String yearPeriod = workElement.findElement(By.cssSelector("td[data-title=\"Duration\"]")).getText();
                if (yearPeriod.equals("N/A")) {
                    workExperience.setYearStart(-1);
                    workExperience.setYearEnd(-1);
                } else {
                    String yearStartRaw = StringUtils.substringBefore(yearPeriod, " - ");
                    workExperience.setYearStart(Integer.parseInt(yearStartRaw));
                    String yearEndRaw = StringUtils.substringAfter(yearPeriod, " - ");
                    if (yearEndRaw.contains("Present") || yearEndRaw.equals("")) {
                        workExperience.setYearEnd(0);
                    } else {
                        workExperience.setYearEnd(Integer.parseInt(yearEndRaw));
                    }
                }
                workExperienceList.add(workExperience);
            }
            attorney.getWorkExperienceList().clear();
            attorney.getWorkExperienceList().addAll(workExperienceList);
        }
    }

    private void scrapeAssociations(Attorney attorney, By associationSelector) {

        if (driver.exists(associationSelector)) {

            By moreSelector = By.cssSelector("#js-v-full-resume > div:nth-child(3) > table > tfoot > tr > td > a");
            clickMore(moreSelector);

            List<Association> associationList = new ArrayList<>();
            List<WebElement> associationElementList = driver.getWebDriver().findElements(By.cssSelector("#js-v-full-resume > div:nth-child(3) > table > tbody > tr"));
            for (WebElement associationElement : associationElementList) {
                Association association = new Association();
                association.setAttorney(attorney);
                String associationName = associationElement.findElement(By.cssSelector("th[scope=\"row\"]")).getText();
                association.setAssociationName(associationName);
                String position = associationElement.findElement(By.cssSelector("td[data-title=\"Position name\"]")).getText();
                association.setPosition(position);
                String yearPeriod = associationElement.findElement(By.cssSelector("td[data-title=\"Duration\"]")).getText();
                if (yearPeriod.equals("N/A")) {
                    association.setYearStart(-1);
                    association.setYearEnd(-1);
                } else {
                    String yearStartRaw = StringUtils.substringBefore(yearPeriod, " - ");
                    association.setYearStart(Integer.parseInt(yearStartRaw));
                    String yearEndRaw = StringUtils.substringAfter(yearPeriod, " - ");
                    if (yearEndRaw.contains("Present") || yearEndRaw.equals("")) {
                        association.setYearEnd(0);
                    } else {
                        association.setYearEnd(Integer.parseInt(yearEndRaw));
                    }
                }
                associationList.add(association);
            }
            attorney.getAssociationList().clear();
            attorney.getAssociationList().addAll(associationList);
        }
    }

    private int scrapeEducationList(Attorney attorney, int nthChildNum) {
        By educationOrPublicationSelector = By.cssSelector("#js-v-full-resume > div:nth-child(" + nthChildNum + ") > table > tbody");
        if (driver.exists(educationOrPublicationSelector)) {
            String educationOrPublicationElementTitle = driver.getWebDriver().findElement(By.cssSelector("#js-v-full-resume > div:nth-child(4) > table > thead > tr > th:nth-child(1)")).getText();
            if (educationOrPublicationElementTitle.contains("Publication")) {
                nthChildNum++;
            }
            By moreSelector = By.cssSelector("#js-v-full-resume > div:nth-child(" + nthChildNum + ") > table > tfoot > tr > td > a");
            clickMore(moreSelector);

            List<Education> educationList = new ArrayList<>();
            List<WebElement> educationElementList = driver.getWebDriver().findElements(By.cssSelector("#js-v-full-resume > div:nth-child(" + nthChildNum + ") > table > tbody > tr"));
            for (WebElement educationElement : educationElementList) {
                Education education = new Education();
                education.setAttorney(attorney);
                String schoolName = educationElement.findElement(By.cssSelector("th[scope=\"row\"]")).getText();
                education.setSchoolName(schoolName);
                String degree = educationElement.findElement(By.cssSelector("td[data-title=\"Degree\"]")).getText();
                education.setDegree(degree);
                String yearGraduated = educationElement.findElement(By.cssSelector("td[data-title=\"Graduated\"]")).getText();
                if (yearGraduated.equals("N/A")) {
                    education.setYearGraduated(-1);
                } else {
                    education.setYearGraduated(Integer.parseInt(yearGraduated));
                }
                educationList.add(education);
            }
            attorney.getEducationList().clear();
            attorney.getEducationList().addAll(educationList);
        }
        nthChildNum++;
        return nthChildNum;
    }

    private void scrapeSpeakingEngagement(Attorney attorney, By speakingEngagementSelector, int nthChildNum) {

        if (driver.exists(speakingEngagementSelector)) {
            By moreSelector = By.cssSelector("#js-v-full-resume > div:nth-child(" + nthChildNum + ") > table > tfoot > tr > td > a");
            clickMore(moreSelector);

            List<SpeakingEngagements> speakingEngagementsList = new ArrayList<>();
            List<WebElement> speakingEngagementsElementList = driver.getWebDriver().findElements(By.cssSelector("#js-v-full-resume > div:nth-child(" + nthChildNum + ") > table > tbody > tr"));
            for (WebElement speakingEngagementElement : speakingEngagementsElementList) {
                SpeakingEngagements speakingEngagement = new SpeakingEngagements();
                speakingEngagement.setAttorney(attorney);
                String conferenceName = speakingEngagementElement.findElement(By.cssSelector("th[scope=\"row\"]")).getText();
                speakingEngagement.setConferenceName(conferenceName);
                String title = speakingEngagementElement.findElement(By.cssSelector("td[data-title=\"Title\"]")).getText();
                speakingEngagement.setTitle(title);
                String year = speakingEngagementElement.findElement(By.cssSelector("td[data-title=\"Date\"]")).getText();
                if (year.equals("N/A")) {
                    speakingEngagement.setYear(-1);
                } else {
                    speakingEngagement.setYear(Integer.parseInt(year));
                }
                speakingEngagementsList.add(speakingEngagement);
            }
            attorney.getSpeakingEngagementsList().clear();
            attorney.getSpeakingEngagementsList().addAll(speakingEngagementsList);
        }
    }

    private void clickMore(By moreSelector) {
        if (driver.exists(moreSelector)) {
            WebElement moreButton = driver.getWebDriver().findElement(moreSelector);
            driver.delay(2000, 2500);
            moreButton.click();
            driver.delay(5000, 7000);
        }
    }

    private void scrapeBio(Attorney attorney, By aboutAttorneySelector) {
        if (driver.exists(aboutAttorneySelector)) {
            driver.scrollToElement(aboutAttorneySelector);
            By moreSelector = By.cssSelector("#about div > a");
            clickMore(moreSelector);

            String bioAttorney = driver.getWebDriver().findElement(By.cssSelector("div#js-truncated-aboutme")).getText();
            attorney.setBio(bioAttorney);
        }
    }

    private void scrapePaidAccount(Attorney attorney, By proStatusSelector) {

        if (driver.exists(proStatusSelector)) {
            attorney.setPaidUser(Boolean.TRUE);
        }
    }

    private void scrapeReviewsBySelector(Attorney attorney, By reviewsSelector) {
        if (driver.exists(reviewsSelector)) {
            driver.scrollToElement(reviewsSelector);
            scrapeRating(attorney);
        }
    }

    private AntiCaptureCreateTaskResponseDTO createAntiCaptureTask(String websiteURL, String dataSitekey) {
        TaskRequestDTO task = new TaskRequestDTO(TYPE, websiteURL, dataSitekey);
        AntiCaptureCreateTaskRequestDTO taskRequestDTO = new AntiCaptureCreateTaskRequestDTO(CLIENT_KEY, task);

        AntiCaptureCreateTaskResponseDTO taskResponseDTO = new AntiCaptureCreateTaskResponseDTO();
        try {
            taskResponseDTO = fetchTaskId(taskRequestDTO);
        } catch (HttpClientErrorException ex) {
            logger.info("Could not connect to antiCapture server");
        }
        return taskResponseDTO;
    }

    private AntiCaptureCreateTaskResponseDTO fetchTaskId(AntiCaptureCreateTaskRequestDTO taskRequestDTO) throws HttpClientErrorException {
        final RestTemplate restTemplate = new RestTemplate();

        HttpEntity<AntiCaptureCreateTaskRequestDTO> body = new HttpEntity<>(taskRequestDTO);
        ResponseEntity<AntiCaptureCreateTaskResponseDTO> exchange = restTemplate.exchange(
                ANTI_CAPTURE_CREATE_TASK_URL,
                HttpMethod.POST, body,
                new ParameterizedTypeReference<AntiCaptureCreateTaskResponseDTO>() {
                });

        AntiCaptureCreateTaskResponseDTO resultDTO = exchange.getBody();
        logger.info("Creating task completed");
        return resultDTO;
    }

    private AntiCaptureResultResponseDTO getAntiCaptureResponse(AntiCaptureCreateTaskResponseDTO taskResponseDTO) {
        AntiCaptureResultResponseDTO captureResultResponseDTO = new AntiCaptureResultResponseDTO();

        try {
            do {
                Thread.sleep(15000);
                captureResultResponseDTO = fetchAntiCaptureResponse(taskResponseDTO);
            } while (!"ready".equals(captureResultResponseDTO.getStatus()));
        } catch (HttpClientErrorException ex) {
            logger.info("Could not connect to antiCapture server");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return captureResultResponseDTO;
    }

    private AntiCaptureResultResponseDTO fetchAntiCaptureResponse(AntiCaptureCreateTaskResponseDTO taskResponseDTO) throws HttpClientErrorException {
        final RestTemplate restTemplate = new RestTemplate();

        AntiCaptureRequestDTO captureRequestDTO = new AntiCaptureRequestDTO(CLIENT_KEY, taskResponseDTO.getTaskId());

        HttpEntity<AntiCaptureRequestDTO> body = new HttpEntity<>(captureRequestDTO);
        ResponseEntity<AntiCaptureResultResponseDTO> exchange = restTemplate.exchange(
                ANTI_CAPTURE_RESPONSE_URL,
                HttpMethod.POST, body,
                new ParameterizedTypeReference<AntiCaptureResultResponseDTO>() {
                });

        AntiCaptureResultResponseDTO resultDTO = exchange.getBody();
        logger.info("Response got");
        return resultDTO;
    }

    public void makeSomeMagic(String key) {
        if (driver.getWebDriver() instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver.getWebDriver()).executeScript("handleCaptcha(\'" + key + "\')");
        }
    }
}
