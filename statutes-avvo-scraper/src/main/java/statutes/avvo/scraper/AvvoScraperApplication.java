package statutes.avvo.scraper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import statutes.avvo.scraper.service.AttorneyService;

@SpringBootApplication
public class AvvoScraperApplication implements CommandLineRunner {

    @Autowired
    private AttorneyService attorneyService;

    public static void main(String[] args) {
        SpringApplication.run(AvvoScraperApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        attorneyService.scrapeAttorney();
    }
}
