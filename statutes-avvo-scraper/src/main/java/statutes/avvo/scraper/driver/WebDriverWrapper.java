package statutes.avvo.scraper.driver;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static statutes.avvo.scraper.constant.Constants.CHROME_DRIVER_PATH;
import static statutes.avvo.scraper.constant.Constants.DRIVER_PROPERTY_NAME;

@Scope(value = "prototype", proxyMode = ScopedProxyMode.DEFAULT)
@Component
public class WebDriverWrapper {
    private WebDriver webDriver;

    @PostConstruct
    private void init() {
        System.setProperty(DRIVER_PROPERTY_NAME, new ClassPathResource(CHROME_DRIVER_PATH).getPath());
        webDriver = new ChromeDriver();
    }

    @PreDestroy
    private void destroy() {
        webDriver.close();
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void sendKeys(By by, String keysToSend) {
        WebElement element = webDriver.findElement(by);
        for (char value : keysToSend.toCharArray()) {
            element.sendKeys(Character.toString(value));
            delay(200, 2000);
        }
    }

    public boolean exists(By by) {
        return webDriver.findElements(by).size() != 0;
    }

    public boolean existsIn(By cssSelector, WebElement webElement) {
        return webElement.findElements(cssSelector).size() != 0;
    }

    public void clearInput(By by) {
        final WebElement inputField = webDriver.findElement(by);
        while (!inputField.getAttribute("value").equals("")) {
            inputField.sendKeys(Keys.BACK_SPACE);
            delay(100, 200);
        }
    }

    public void scrollToElement(By by) {
        WebElement element = webDriver.findElement(by);
        ((ChromeDriver) webDriver).executeScript("arguments[0].scrollIntoView({block: 'end', behavior: 'smooth'});", element);
    }

    public void delay(int min, int max) {
        long delayValue = (long) (min + Math.random() * (max - min));
        try {
            Thread.sleep(delayValue);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}