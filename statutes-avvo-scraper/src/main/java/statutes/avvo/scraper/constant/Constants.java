package statutes.avvo.scraper.constant;

/**
 * @author foxy
 * @since 24.07.18.
 */
public class Constants {
    public static final String AVVO_START_PAGE = "https://www.avvo.com/find-a-lawyer";
    public static final String FIREFOX_AGENT = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0";
    public static final String CHROME_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36";
    public static final String CHROME_DRIVER_PATH = "src/main/resources/driver/chromedriver";
    public static final String DRIVER_PROPERTY_NAME = "webdriver.chrome.driver";

}
