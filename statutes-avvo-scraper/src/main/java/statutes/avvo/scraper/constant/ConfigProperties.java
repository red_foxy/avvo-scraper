package statutes.avvo.scraper.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigProperties {

    private String avvoLogin;
    private String avvoPassword;

    public String getAvvoLogin() {
        return avvoLogin;
    }

    @Value("${avvo.login}")
    public void setAvvoLogin(String avvoLogin) {
        this.avvoLogin = avvoLogin;
    }

    public String getAvvoPassword() {
        return avvoPassword;
    }

    @Value("${avvo.password}")
    public void setAvvoPassword(String avvoPassword) {
        this.avvoPassword = avvoPassword;
    }
}