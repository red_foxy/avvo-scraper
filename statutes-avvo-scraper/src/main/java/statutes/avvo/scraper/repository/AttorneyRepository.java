package statutes.avvo.scraper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import statutes.avvo.scraper.model.Attorney;

/**
 * @author foxy
 * @since 26.07.18.
 */
@Repository
public interface AttorneyRepository extends JpaRepository<Attorney, Long>  {
}
