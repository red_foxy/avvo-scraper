package statutes.avvo.scraper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import statutes.avvo.scraper.model.url.URLContainer;

import java.util.List;

/**
 * @author foxy
 * @since 25.07.18.
 */
@Repository
public interface URLContainerRepository extends JpaRepository<URLContainer, Long> {

    List<URLContainer> findAllByIsScrapedFalse();
}
