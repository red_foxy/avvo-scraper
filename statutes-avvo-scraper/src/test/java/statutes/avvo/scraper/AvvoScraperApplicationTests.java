package statutes.avvo.scraper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import statutes.avvo.scraper.model.url.URLContainer;
import statutes.avvo.scraper.repository.URLContainerRepository;
import statutes.avvo.scraper.scraper.AttorneyScraper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AvvoScraperApplicationTests {

    @Autowired
    private AttorneyScraper attorneyScraper;

    @Autowired
    private URLContainerRepository urlContainerRepository;

    @Test
    public void contextLoads() {
        URLContainer container = urlContainerRepository.getOne(1L);
        attorneyScraper.scrapeProfile(container);
    }
}
